<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ellipticalreviews
 */

get_header(); ?>
<section role="main">
    <div class="breadcrumbs"><div class="wrapper"><?php get_breadcrumb(); ?></div><hr></div>
   <div class="wrapper">      
		<?php
		while ( have_posts() ) : the_post();
			get_template_part( 'template-parts/content-single', get_post_format() );			
			

		endwhile;
		?>           
  </div>
</section>
            
<?php
get_sidebar();
get_footer();
