<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>


<article class="content" id="post-<?php the_ID(); ?>">
<div class="wrapper" itemscope="" itemtype="https://data-vocabulary.org/Review">
        <div class="row">
                <div class="col-sm-8">
              <?php
		if ( is_single() ) :
			the_title( '<h1 class="h1-line"><span>', '</span></h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;
		 ?>
               <?php echo wpse_78113_get_rating( $post->ID );?>
                                            
           <div class="post-media post-single-media">               
	     <a href="<?php the_permalink(); ?>">
	    <?php if ( has_post_thumbnail() ) {
	      the_post_thumbnail( 'post', array( 'class'  => 'img-responsive aligncenter size-full' ) );
	     } 
		?></a>				
           </div>                            
 <?php the_content(); ?>
                                        
      <?php $orig_post = $post;
    global $post;
    $categories = get_the_category($post->ID);
    if ($categories) {
    $category_ids = array();
    foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
    $args=array(
    'category__in' => $category_ids,
    'post__not_in' => array($post->ID),
    'posts_per_page'=> 8,
    'orderby'   => 'rand',
    'caller_get_posts'=>1
    );
    $my_query = new wp_query( $args );
    if( $my_query->have_posts() ) {
    echo '<div class="quickreview"><h3>Other Sole Ellipticals</h3>';
    while( $my_query->have_posts() ) {
    $my_query->the_post(); ?>  	
<p class="allcats brand_link"><a href="<?php the_permalink(); ?>" class="productlink"><span class="thumb-ico"><?php if ( has_post_thumbnail() ) {the_post_thumbnail( 'post', array( 'class'  => '' ) );} ?></span> <?php the_title(); ?></a></p>                          <?php
    }
    echo '</div>';
    }
    }
    $post = $orig_post;
    wp_reset_query(); ?>  
                                            
    <?php if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;  ?>                                  
</div>
					<div class="col-sm-4">
					
					<div class="sidebar">
                                            
      				<?php get_sidebar(); ?>
    					    				</div>
    			</div>
				</div>
				
				
				
				
		    </div>
		</article>