<?php

/* 
 
Template Name: Review page template
 
*/

get_header();
?>
<section role="main" class="cat-best">
    	<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">				
					<div class="wrapper category-description">
 <?php  while ( have_posts() ) : the_post();		
        the_content();
	endwhile; ?>
</div>
				</div>
			</div>
		</div>
</section>

<?php get_footer();



